import React, { useState, ReactElement } from "react";
import { Game, calculateWinner } from "./Game";
import Square from "./Square";
import "./App.css";

function App(): JSX.Element {
  return <Game />;
}

export default App;
