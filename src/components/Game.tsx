import React, { useState } from "react";
import Board, { BoardState } from "./Board";

function Game() {
  const [history, setHistory] = useState([{ squares: Array(36).fill(null) }]);
  const [stepNumber, setStepNumber] = useState(0);
  const [xIsNext, setXIsNext] = useState(true);

  const handleClick = (i: number) => {
    let newHistory = history.slice(0, stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares, 6, "❌", "⭕") || squares[i]) {
      return;
    }

    squares[i] = xIsNext ? "❌" : "⭕";
    newHistory = newHistory.concat([
      {
        squares
      },
    ]);
    setHistory(newHistory);
    setStepNumber(stepNumber + 1);
    setXIsNext(!xIsNext);
  };

  const jumpTo = (step: number) => {
    setStepNumber(step);
    setXIsNext(step % 2 === 0);
  };

  const current = history[stepNumber];
  const winner = calculateWinner(current.squares, 6, "❌", "⭕");

  console.log(current.squares);

  const moves = history.map((step: any, move: any) => {
    const desc = move ? "Go to move #" + move : "Go to game start";
    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{desc}</button>
      </li>
    );
  });

  let status;
  if (winner) {
    status = "Winner: " + winner;
  } else {
    status = "Next player: " + (xIsNext ? "❌" : "⭕");
  }

  return (
    <div className="container">
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i: number) => handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
      <div className="Passage">
        <h1> Modified 6x6 Tic-tac-toe game using React, Typescript,CSS </h1>
        <h2>Thank you for visiting </h2>
      </div>
    </div>
  );
}

// this function calculates the winner of a tic tac toe game of size nxn
// squares being 'X', 'O' or ' '
function calculateWinner(
  squares: string[],
  dimension: number,
  X: string,
  O: string
): any {
  const rows = new Array(dimension).fill(0);
  const cols = new Array(dimension).fill(0);
  const diag = new Array(2).fill(0);

  // loop over each cell of the board
  for (let row = 0; row < dimension; row++) {
    for (let col = 0; col < dimension; col++) {
      // get the element via index calculation y * width + x
      const square = squares[row * dimension + col];

      // increment for player one
      if (square === X) {
        rows[row]++;
        cols[col]++;
      }
      // decrement for player two
      else if (square === O) {
        rows[row]--;
        cols[col]--;
      }

      // check diagonal
      if (row === col) {
        if (square === X) diag[0]++;
        else if (square === O) diag[0]--;
      }
      // check anti diagonal
      if (row === dimension - col - 1) {
        if (square === X) diag[1]++;
        else if (square === O) diag[1]--;
      }
    }
  }

  // check if any of the rows or columns are completed by either player
  for (let i = 0; i < dimension; i++) {
    // row/col contains the value of the dimension
    // if and only if the whole row/col only contains 'X' values
    // and therefore get incremented each time a cell
    // in that row/col gets looked at above

    if (rows[i] === dimension || cols[i] === dimension) return X;
    else if (rows[i] === -dimension || cols[i] === -dimension) return O;
  }

  // same as with the rows/cols but since there are only two diagonals,
  // do this right here

  if (diag[0] === dimension || diag[1] === dimension) return X;
  else if (diag[0] === -dimension || diag[1] === -dimension) return O;

  // otherwise no winner is found
 
  return null;
}

export { Game, calculateWinner };
